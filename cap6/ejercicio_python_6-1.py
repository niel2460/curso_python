class Vehiculo:

    def __init__(self, color, ruedas, puertas):
        self.color = color
        self.ruedas = ruedas
        self.puertas = puertas

class Coche(Vehiculo):
    def __init__(self, color, ruedas, puertas, velocidad, cilindrada):
        self.color = color
        self.ruedas = ruedas
        self.puertas = puertas
        self.velocidad = velocidad
        self.cilindrada = cilindrada
        
        print("El color del vehiculo es: ", self.color)
        print("La cantidad de ruedas del vehiculo es: ", self.ruedas)
        print("La cantidad de puertas del vehiculo es: ", self.puertas)
        print("La velocidad del vehiculo es: ", self.velocidad)
        print("La cilindrada del vehiculo es: ", self.cilindrada)

c = Coche("rojo",4,5,150,2000)
c = Coche("azul",4,2,100,1600)
