def numero_primo():
    numero = int(input('Ingrese un numero entero: '))

    if numero > 1:
        for indice in range(2,int(numero)):
            if(numero % indice) == 0:
                print(f"El numero {numero} no es primo porque no es divisible entre {indice}")
                break
            else:
                print(f"El numero {numero} es primo")
                break
    else:
        print("los numeros primos son mayores a 1")


numero_primo()
