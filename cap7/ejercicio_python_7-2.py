import time

hora = float(time.strftime('%H'))
minuto = float(time.strftime('%M'))
hora_salida = 19

if(hora)>=hora_salida:
    print("Es hora de salir")
else:
    print("Faltan {} horas y {} minutos para salir".format(hora_salida - hora,59 - minuto))
