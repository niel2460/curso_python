import sqlite3

def tabla():
    conn = sqlite3.connect('miaplicacion.db')
    cursor = conn.cursor()

    #creacion def tabla
    rows = cursor.execute("DROP TABLE Alumnos")
    rows = cursor.execute("CREATE TABLE Alumnos(Id int primary key not null, Nombre text not null, Apellido text not null)")

    #insercion de registros
    rows = cursor.execute("insert into Alumnos values(1,'pedro','peres')")
    rows = cursor.execute("insert into Alumnos values(2,'juan','castro')")
    rows = cursor.execute("insert into Alumnos values(3,'mario','martinez')")
    rows = cursor.execute("insert into Alumnos values(4,'pablo','contreras')")
    rows = cursor.execute("insert into Alumnos values(5,'maria','figueroa')")
    rows = cursor.execute("insert into Alumnos values(6,'martha','gabarrete')")
    rows = cursor.execute("insert into Alumnos values(7,'Alejandra','guzman')")
    rows = cursor.execute("insert into Alumnos values(8,'maria','estrada')")

    conn.commit()
    cursor.close()
    conn.close()

 
def busqueda(nombre):
    conn = sqlite3.connect('miaplicacion.db')
    cursor = conn.cursor()

    query = f'SELECT * FROM Alumnos WHERE nombre="{nombre}"'
    
    if nombre == "":
        query = f'SELECT * FROM ALumnos'

    rows = cursor.execute(query)
    data = rows.fetchall()

    print(query)

    print(data)

    cursor.close()
    conn.close()

def main():
    tabla()
    busqueda('martha')

if __name__ == "__main__":
    main()
