#variables
entero = 5
cadenas = "Hola mundo"
boleano = True
decimales = 5.5
tuplas = ("manzana","banano","uvas")
diccionario = {"verdura1":"tomate","verdura2":"apio","verdura3":"chile dulce"}

#salida por pantalla
print("Hola mundo")

#pedir informacion del usuario
cadena = input("Ingrese un valor: ")

#validar tipo de datos
type(entero)

#condiciones
if(entero == 5):
    print("El numero es igual a 5")
else:
    print("El numero NO es igual a 5")

#ciclo for
for ind in range(entero):
    print(ind)

#ciclo while
numero = int(input('Ingrese un numero del 1 al 9: '))
while(numero < 10):
    print(entero)
    numero = numero+1

#funciones
def suma(a,b):
    print(a+b)

def sumatoria(*args):
    res = 0

    for arg in args:
        res += arg

    print(res)

sumalambda = lambda x:x+x

suma(5,4)
sumatoria(5,4,3,2,1)
print(sumalambda(2))
