import tkinter
from tkinter import ttk
import random

def mifuncion():
    print("clickado")
window = tkinter.Tk()

#label_saludo = tkinter.Label(window, text="Hola", bg="yellow", fg="blue")
#label_saludo.pack(ipadx=50,ipady=50)

#label_adios = tkinter.Label(window, text="Adios", bg="red", fg="white")
#label_adios.pack(ipadx=50,ipady=50)

#(0,0) (1,0) (2,0)
#(0,1) (1,1) (2,1)
#(0,2) (1,2) (2,2)
#(0,3) (1,3) (2,3)

window.columnconfigure(0,weight=1)
window.columnconfigure(1,weight=3)

#etiquetas de usuario
username_label = ttk.Label(window,text="Username:")
username_label.grid(column=0,row=0,sticky=tkinter.W, padx=5, pady=5)

#cuadro de texto de usuario
username_entry = ttk.Entry(window)
username_entry.grid(column=1,row=0,sticky=tkinter.E, padx=5, pady=5)

#etiquetas de password
password_label = ttk.Label(window,text="Password:")
password_label.grid(column=0,row=1,sticky=tkinter.W, padx=5, pady=5)

#cuadro de texto protegito de password
password_entry = ttk.Entry(window,show="*")
password_entry.grid(column=1,row=1,sticky=tkinter.E, padx=5, pady=5)

#boton
login_button = ttk.Button(window,text="Login")
login_button.grid(column=1,row=2,sticky=tkinter.E, padx=5, pady=5)

label1 = tkinter.Label(window,text="Posisionamiento absoluto", bg="blue",fg="white")
label1.place(x=10,y=100)

label2 = tkinter.Label(window,text="Otro mas", bg="red",fg="yellow")
label2.place(x=10,y=150,relwidth=0.5,anchor="ne")


colors = ['blue','red','yellow','purple','green','black']

for x in range(0,10):
          color = random.randint(0,len(colors)-1)
          color2 = random.randint(0,len(colors)-1)
          label = tkinter.Label(window,text="etiqueta", bg=colors[color], fg=colors[color2])
          label.place(x=random.randint(0,100), y = random.randint(0,100))

#objeto para agrupar elementos
frame = ttk.Frame(window)
print(dir(frame))
frame['relief']= 'sunken'

#listbox
lista = ['window','mac','Linux','os/2']
lista_items = tkinter.StringVar(value=lista)
listbox = tkinter.Listbox(window,height=10, listvariable=lista_items)
listbox.grid(column=0, row=0, sticky=tkinter.W)

#radio boton
seleccionado = tkinter.StringVar()
r1 = ttk.Radiobutton(window, text="si", value='1', variable=seleccionado)
r2 = ttk.Radiobutton(window, text="no", value='2', variable=seleccionado)
r3 = ttk.Radiobutton(window, text="quizas", value='3', variable=seleccionado)

r1.grid(column=0, row=1, padx=5,pady=5)
r2.grid(column=0, row=2, padx=5,pady=5)
r3.grid(column=0, row=3, padx=5,pady=5)

#check box
seleccionado2 = tkinter.StringVar()
checkbox = ttk.Checkbutton(window, text="acepto las condiciones", variable=seleccionado2, command=mifuncion)
checkbox.grid(row=0, column=0)

window.mainloop()


window = tkinter.Tk()

def saludar(event):
    print("Han hecho click en saludar")

def saludardobleclick(event):
    print("Han hecho doble click en saludar")

boton = tkinter.Button(window, text="Has click")
boton.pack()
boton.bind('<Button-1>',saludar)
boton.bind('<Button-1>',saludardobleclick)
window.mainloop()

from tkinter import filedialog
window = tkinter.Tk()
filename = filedialog.askopenfilename()
window.mainloop()
