from django.db import models
from django.urls import reverse

# Create your models here.

class Genre(models.Model):
    name = models.CharField(max_length=64, help_text="pon el nombre del genero:")

    def __str__(self) :
        return self.name


class Book(models.Model):
    title = models.CharField(max_length=32)
    author = models.ForeignKey('Autor', on_delete=models.SET_NULL,null=True)
    summary = models.TextField(max_length=100,help_text="Pon aqui de que va el libro")
    isbn = models.CharField('ISBN',max_length=13,help_text="El ISBN del libro de 13 caracteres")
    genre = models.ManyToManyField(Genre)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('book-detail',args=[str(self.id)])