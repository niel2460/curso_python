#clases
class Juguete:
    _encendido = False
    
    def enciende(self):
        print('Enciende el jueguete')
        self._encendido = True

    def apaga():
        print('Apaga el juguete')
        self._encendido = False

    def IsEncendido(self):
        return self._encendido;


d = Juguete()
d.encendido = True
print(d.enciende())

#clase estatica
class Estatica:
    numero = 1

    def incrementa():
        Estatica.numero+=1

Estatica.incrementa()
print(Estatica.numero)

#herencia de clase
class Potato(Juguete):
    _encendido = False

    #declaracion de construrtor
    def __init__(self, Nombre):
        print('estoy en el constructor')

    #declaracion de destructor, aunque el recolector de basura se encarga de eso    
    def __del__(self):
        print('Estoy en el destructor', self.__class__)


    def enciende(self):
        self._encienddo = True

    def apaga(self):
        self._encienddo = False

    def IsEncendido(self):
        return self._encienddo

p = Potato('Alex')
p.enciende()
print(p.IsEncendido())
p.apaga()
print(p.IsEncendido())
del(p)

#clase abstracta
from abc import ABC,abstractmethod
class Animal(ABC):
    @abstractmethod
    
    def sonido(self):
        pass
    def diHola(self):
        print("Hola")


class Perro(Animal):
    def sonido(self):
        print("Guau")

class Gato(Animal):
    def sonido(self):
        print("Miau")

p = Perro()
p.sonido()
p.diHola()

g = Gato()
g.sonido()
g.diHola()

#ejemplo de clase coche
class Motor:
    tipo = "Diesel"

class Ventanas:
    cantidad = 5

    def cambiarCantidad(self, valor):
        self.cantidad = valor

class Ruedas:
    cantidad = 4

class Carroceria:
    ventanas = Ventanas()
    ruedas = Ruedas()

class Coche:
    motor = Motor()
    carroceria = Carroceria()

c = Coche()
print("Motor es: ", c.motor.tipo)
print("Ventanas: ", c.carroceria.ventanas.cantidad)

c.carroceria.ventanas.cambiarCantidad(3)
print("Ventanas: ", c.carroceria.ventanas.cantidad)